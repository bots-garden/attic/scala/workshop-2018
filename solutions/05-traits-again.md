# Solutions

```scala
// ### 05-traits-again ###
package demo

object powers {
  trait RunFast {
    def nickName: String

    def run() = {
      println(s"$nickName runs fast")
    }
  }
  trait GoToTheFuture {
    def nickName: String

    def fastForward() = {
      println(s"$nickName runs to the future")
    }
  }
  trait NickName {
    var nickName: String = ""
  }
}


object people {
  

  class Creature {
    val id: String = java.util.UUID.randomUUID.toString
  }

  class Human(val name: String = "John Doe") extends Creature {
    def hello() = {
      println(s"Hello I'm $name")
    }

    def say(something: String) = {
      println(s"🤖 $something")
    }
  }

}

import powers._
import people._

object Hello extends App {

  val kidFlash = new Human("Wally") 
    with NickName 
    with RunFast 
    with GoToTheFuture

  kidFlash.nickName = "Kid Flash"
  
  kidFlash.run // "Kid Flash runs fast"
  
  kidFlash.fastForward // "Kid Flash runs to the future"

}
```