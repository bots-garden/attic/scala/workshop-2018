# Solutions

```scala
// ### 04-traits ###
package demo

object powers {
  trait RunFast {
    def nickName: String

    def run() = {
      println(s"$nickName runs fast")
    }
  }
  trait GoToTheFuture {
    def nickName: String

    def fastForward() = {
      println(s"$nickName runs to the future")
    }
  }
}


object people {
  

  class Creature {
    val id: String = java.util.UUID.randomUUID.toString
  }

  class Human(val name: String = "John Doe") extends Creature {
    def hello() = {
      println(s"Hello I'm $name")
    }

    def say(something: String) = {
      println(s"🤖 $something")
    }
  }
  /**
   * we don't know in advance the power of the superhero in "The Flash"
   */

  class SuperHero(name: String, val nickName: String) extends Human(name) {
    // empty class
  }  
  
  import powers._

  class Speedster(name: String, nickName: String) 
    extends SuperHero(name, nickName) 
    with RunFast 
    with GoToTheFuture {

  }
  
}

import people._

object Hello extends App {
  val flash = new Speedster("Barry Allen", "The Flash")

  flash.run() // "The Flash runs fast"
  flash.fastForward() // "The Flash runs to the future"

}
```

