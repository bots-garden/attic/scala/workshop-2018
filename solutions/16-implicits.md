# Solutions

```scala
package demo

// simplify
object animals {
	class Animal(val name: String) { // val => immutable property ⚠️
	  def hello() = {
	    println(s"Hello I'm $name") // string interpolation
	  }
	}
	
	class Dog(name: String) extends Animal(name) {
	  def wouaf() = {
	    println("wouaf I'm a 🐶")
	  }
	}
}

object magic {
	implicit class AnimalPouetPouet(val animal: animals.Animal) {
	  def hey: Unit = { println(s"Hey 👋 I'm ${animal.name}") }
	}
}


import animals._
import magic._

object Hello extends App {

	val wolf = new Dog("wolf")
	wolf.wouaf

	wolf.hey

}

```