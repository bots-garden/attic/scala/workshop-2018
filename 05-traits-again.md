# 05-traits-again: Scala is dynamic 😉

Il est possible "d'ajouter" des traits au moment de la création d'une instance de classe:

```scala
trait Country {
  var country: String = "FR"
}

val bob = new Human("Bob") with Country
println(bob.country) // FR
bob.country = "UK"
```

On peut donc créer des "super héros" sans hériter de `SuperHero`

## Exercice

- ▶️ https://scastie.scala-lang.org/k33g/6Dtx3rECSciYZffL5nhw1Q

- Complétez `/people/NickName` pour ajouter une propriété `nickName`
- Complétez `/demo/Hello` pour que **Wally** devienne **Kid Flash**